" make using docker
setlocal errorformat+=%E!\ LaTeX\ %trror:\ %m,
                    \%E!\ %m,
                    \%+WLaTeX\ %.%#Warning:\ %.%#line\ %l%.%#,
                    \%+W%.%#\ at\ lines\ %l--%*\\d,
                    \%WLaTeX\ %.%#Warning:\ %m,
                    \%Cl.%l\ %m,
                    \%+C\ \ %m.,
                    \%+C%.%#-%.%#,
                    \%+C%.%#[]%.%#,
                    \%+C[]%.%#,
                    \%+C%.%#%[{}\\]%.%#,
                    \%+C<%.%#>%.%#,
                    \%C\ \ %m,
                    \%-GSee\ the\ LaTeX%m,
                    \%-GType\ \ H\ <return>%m,
                    \%-G\ ...%.%#,
                    \%-G%.%#\ (C)\ %.%#,
                    \%-G(see\ the\ transcript%.%#),
                    \%-G\\s%#,
                    \%+O(%f)%r,
                    \%+P(%f%r,
                    \%+P\ %\\=(%f%r,
                    \%+P%*[^()](%f%r,
                    \%+P[%\\d%[^()]%#(%f%r,
                    \%+Q)%r,
                    \%+Q%*[^()])%r,
                    \%+Q[%\\d%*[^()])%r

setlocal makeprg=docker\ run\ --rm\ -v\ $PWD:/home/latex\ latex

" snippets
nnoremap ,item :-1read $HOME/.vim/snippets/tex/itemize<CR>ji
nnoremap ,enum :-1read $HOME/.vim/snippets/tex/enumerate<CR>ji
nnoremap ,sec :-1read $HOME/.vim/snippets/tex/section<CR>f{a
nnoremap ,ssec :-1read $HOME/.vim/snippets/tex/subsection<CR>f{a
nnoremap ,sssec :-1read $HOME/.vim/snippets/tex/subsubsection<CR>f{a
