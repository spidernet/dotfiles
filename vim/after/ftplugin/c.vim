" Ignore indents caused by parentheses in FreeBSD style.
function! IgnoreParenIndent()
    let indent = cindent(v:lnum)

    if indent > 4000
        if cindent(v:lnum - 1) > 4000
            return indent(v:lnum - 1)
        else
            return indent(v:lnum - 1) + 4
        endif
    else
        return (indent)
    endif
endfun

" FreeBSD Style(9)
setlocal cindent
setlocal cinoptions=(4200,u4200,+0.5s,*500,:0,t0,U4200
setlocal indentexpr=IgnoreParenIndent()
setlocal indentkeys=0{,0},0),:,0#,!^F,o,O,e
setlocal noexpandtab
setlocal tabstop=8
setlocal textwidth=80

set list lcs=tab:>-

" astyle formatprg
setlocal formatprg=indent

" auto-create folds per grammar
setlocal foldlevel=10

" local project headers
setlocal path=.,,*/include/**3,./*/include/**3
" basic system headers
setlocal path+=/usr/include

" snippets
nnoremap ,mainf :-1read $HOME/.vim/snippets/C/mainf<CR>2jo
nnoremap ,main :-1read $HOME/.vim/snippets/C/main<CR>5jo
nnoremap ,inc :-1read $HOME/.vim/snippets/C/inc<CR>f.i
nnoremap ,include :-1read $HOME/.vim/snippets/C/include<CR>f.i
iabbrev #g _<c-r>=expand("%:t:r")<cr><esc>VgUV:s/[^A-Z]//g<cr>A_H<esc>yypki
            \#ifndef <esc>j0i#define <esc>o<cr><cr>#endif<esc>2ki
