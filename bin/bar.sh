#!/bin/bash

MAINFONT="6x10"
ICONFONT="-wuncon-siji-medium-r-normal--10-100-75-75-c-80-iso10646-1"
BARHEIGHT=12
FGCOLOR="#a0a0a0"
BGCOLOR="#262626"

clock() {
	TIME=`date +"%a %b %Y %T"`
	echo -e "$TIME"
}

groups() {
    # print current group and shown groups
    cur=`xprop -root _NET_CURRENT_DESKTOP | awk '{print $3}'`

    for wid in `xprop -root | sed '/_LIST(WINDOW)/!d;s/.*# //;s/,//g'`; do
        grp=`xprop -id $wid _NET_WM_DESKTOP | awk '{print $3}'`
        shown="$shown $grp"
    done

    shown=`echo $shown | tr " " "\n" | sort -g | uniq`

    for g in `seq 1 9`; do
        if test $g == $cur; then
            groups="${groups} [$g] "
        elif grep -q $g <<< "$shown"; then
            groups="${groups} $g "
        else
            groups="${groups}"
        fi
    done

	echo -e " $groups"
}

title() {
    curwin=`xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | cut -f 2`

    if [ $curwin != "0x0" ]
    then
        curwin=`xprop -id ${curwin} _NET_WM_NAME | awk -F '"' '{print $2}' | cut -c -80`
    else
        curwin=""
    fi

    echo -e "$curwin"
}


battery() {
	BATC=`cat /sys/class/power_supply/BAT0/capacity`
	CHRG=`cat /sys/class/power_supply/BAT0/status`

    if (( $BATC == 100)); then
        BICO="\ue254"
    elif (( $BATC >= 90 && $BATC < 100)); then
	    BICO="\ue253"
    elif (( $BATC >= 80 && $BATC < 90 )); then
	    BICO="\ue252"
    elif (( $BATC >= 70 && $BATC < 80 )); then
	    BICO="\ue251"
    elif (( $BATC >= 60 && $BATC < 70 )); then
	    BICO="\ue250"
    elif (( $BATC >= 50 && $BATC < 60 )); then
	    BICO="\ue24f"
    elif (( $BATC >= 40 && $BATC < 50 )); then
	    BICO="\ue24e"
    elif (( $BATC >= 30 && $BATC < 40 )); then
	    BICO="\ue24d"
    elif (( $BATC >= 20 && $BATC < 30 )); then
	    BICO="\ue24c"
    else
        BICO="\ue242"
	fi

	case "$CHRG" in
	Charging)
        BSTAT="C"
        ;;
    Discharging)
        BSTAT="D"
        ;;
    Not\ charging)
        BSTAT="N"
        ;;
	esac
	
	echo -e "$BICO $BSTAT $BATC%"
}

tasks() {
    total=`task +READY count`
    today=`task +READY +TODAY count`
    tomorrow=`task +READY +DUETOMORROW count`
    urgent=`task +READY urgency \> 10 count`
    overdue=`task +READY +OVERDUE count`

    tasks="\ue1f2 $total"

    if [ $today -gt 0 ]; then
        tasks="${tasks} \ue225 $today"
    fi
    if [ $tomorrow -gt 0 ]; then
        tasks="${tasks} \ue226 $tomorrow"
    fi
    if [ $urgent -gt 0 ]; then
        tasks="${tasks} \ue227 $urgent"
    fi
    if [ $overdue -gt 0 ]; then
        tasks="${tasks} \ue076 $overdue"
    fi

    echo -e "$tasks"
}

if pidof lemonbar > /dev/null; then
    killall lemonbar
fi

while true; do
    monitors=$(xrandr | grep -o "^.* connected" | sed "s/ connected//" | wc -l)
    panel_layout="%{l}$(groups)  $(title)  %{r} $(tasks) | $(battery) | $(clock) "

    if [ $monitors -gt 1 ]; then
        echo "%{Sl}${panel_layout}%{Sf}${panel_layout} "
    else
        panel_layout="%{l}$(groups)  $(title)  %{r} $(tasks) | $(battery) | $(clock) "
        echo "${panel_layout} "
    fi

	sleep .25
done | lemonbar -d -g x"${BARHEIGHT}" -B "${BGCOLOR}" -F "${FGCOLOR}" -f "$MAINFONT" -f "$ICONFONT" | sh > /dev/null 2>&1
